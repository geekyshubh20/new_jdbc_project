package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Statementjdbc {

	
	public static void main(String[] args) {
		dynamicval(17, "Navin", "Behal", "nav@gmail.com");
	}
	
	public static void dynamicval(int id,String fname , String lname,String email) {
		Connection conn = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root","abcd");
			PreparedStatement ptsmt = conn.prepareStatement("insert into users (id , first_name , last_name ,email_id) values(?,?,?,?)");
			ptsmt.setInt(1, id);
			ptsmt.setString(2, fname);
			ptsmt.setString(3, lname);
			ptsmt.setString(4, email);
		
			boolean result = ptsmt.execute();
			System.out.println("Result----->>>>"+result);
			
		
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			if(conn!=null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
