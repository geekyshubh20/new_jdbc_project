package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Switchjdbc {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		while (true) {
			System.out.println("Enter 1 for insertion");
			System.out.println("Enter 2 for update");
			System.out.println("Enter 3 for delete");
			System.out.println("Enter 4 for reading");
			System.out.println("Choose Option----------->>>>>");

			int nextInt = scn.nextInt();
			switch (nextInt) {
			case 1: {
				System.out.println("Inside insertion");
				System.out.println("Enter unique id");
				int id = scn.nextInt();
				System.out.println("Enter fname");
				String fname = scn.next();
				System.out.println("Enter lastname");
				String lname = scn.next();
				System.out.println("Enter email");
				String email = scn.next();
				addmember(id, fname, lname, email);
				break;
			}
			case 2: {
				System.out.println("Inside updation");
				System.out.println("Enter unique id");
				int id = scn.nextInt();
				System.out.println("Enter email");
				String email = scn.next();
				updateMember(id, email);
				break;

			}
			case 3: {
				System.out.println("Inside deletion");
				System.out.println("Enter unique id");
				int id = scn.nextInt();
				deleteMember(id);
				break;
			}

			case 4: {
				System.out.println("Inside read");
				readMember();
				break;
			}

			default: {
				break;
			}

			}
			System.out.println("You want to continue ? yes or no");
			String input = scn.next();
			// char charAt = input.charAt(0);

			if (!input.equalsIgnoreCase("y") && (!input.equalsIgnoreCase("yes"))) {
				break;
			}
		}

		scn.close();

	}

	public static void addmember(int id, String fname, String lname, String email) {
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn
					.prepareStatement("insert into users(id , first_name,last_name,email_id) values(?,?,?,?)");
			pstmt.setInt(1, id);
			pstmt.setString(2, fname);
			pstmt.setString(3, lname);
			pstmt.setString(4, email);

			boolean res = pstmt.execute();
			System.out.println("Result is -----" + res);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void updateMember(int id, String email) {
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn.prepareStatement("update users set email_id = ? where id =?");
			pstmt.setString(1, email);
			pstmt.setInt(2, id);

			int executeUpdate = pstmt.executeUpdate();
			if (executeUpdate == 1) {
				System.out.println("updated successfully");
			} else {
				System.out.println("updation failed");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteMember(int id) {
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();
			PreparedStatement pstmt = conn.prepareStatement("delete from users where id =?");
			pstmt.setInt(1, id);

			int executeUpdate = pstmt.executeUpdate();
			if (executeUpdate == 1) {
				System.out.println("Deletion successfully");
			} else {
				System.out.println("Deletion failed");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void readMember() {
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn.prepareStatement("select * from users");

			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String fname = rs.getString("first_name");
				String lname = rs.getString("last_name");
				String email = rs.getString("email_id");
				System.out.println("id=" + id + ", fName=" + fname + ", email=" + email + ", lName=" + lname);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
