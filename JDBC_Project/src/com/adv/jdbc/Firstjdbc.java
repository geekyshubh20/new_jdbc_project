package com.adv.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Firstjdbc {

	public static void main(String[] args) {
		Connection conn = null;
		try {
			// 1.Load Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// 2.Establish Connection
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "abcd");
			// 3.Create Statement
			Statement stmt = conn.createStatement();
			// 4.Execute Query
			ResultSet rs = stmt.executeQuery("Select * from users");
			// 5.iterate the resultset
//			String col1 = rs.getMetaData().getColumnName(1);
//			String col2 = rs.getMetaData().getColumnName(2);
//			String col3 = rs.getMetaData().getColumnName(3);
//			String col4 = rs.getMetaData().getColumnName(4);
//			System.out.println("col1=" + col1 + ", col2=" + col2 + ", col3=" + col3 + ", col4=" + col4);

			while (rs.next()) {
				int id = rs.getInt("id");
				String fname = rs.getString("first_name");
				String lname = rs.getString("last_name");
				String email = rs.getString("email_id");
				System.out.println("id=" + id + ", fName=" + fname + ", email=" + email + ", lName=" + lname);
			}

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {

			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}

	}

}
