package com.adv.jdbc_assignment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.adv.jdbc.ConnectionBridge;

public class StudentConn {

	public void crudOperation() {
		Scanner scn = new Scanner(System.in);
		while (true) {
			System.out.println("Choose operation 1-insertion , 2-updation , 3-deletion, 4-fetch the data");

			int nextInt = scn.nextInt();
			switch (nextInt) {
			case 1: {
				addStudent(scn);
				break;
			}
			case 2: {
				updateStudent(scn);
				break;
			}
			case 3: {
				deleteStudent(scn);
				break;
			}
			case 4: {
				System.out.println("Getting the students data----->>>>");
				getStudentDetails();
				break;
			}
			default: {
				break;
			}
		}
			System.out.println("You want to continue ? yes or no");
			String input = scn.next();
			if (!input.equalsIgnoreCase("y") && (!input.equalsIgnoreCase("yes"))) {
				break;
			}
		}
		scn.close();
	}
	

	private void getStudentDetails() {
		Student[] getstudent = getstudent();
		PrintStudent printStudent = new PrintStudent();
		printStudent.printall(getstudent);
	}

	public static void addStudent(Scanner scn) {
		System.out.println("Insert the datas");
		System.out.println("Enter unique id--------->>>>");
		int id = scn.nextInt();
		System.out.println("Enter Name----->>>");
		String name = scn.next();
		System.out.println("Enter age----->>>");
		int age = scn.nextInt();
		System.out.println("Enter email---->>>>>");
		String email = scn.next();
		Connection conn = null;

		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn
					.prepareStatement("insert into student(id , name ,age ,email) values(?,?,?,?)");
			pstmt.setInt(1, id);
			pstmt.setString(2, name);
			pstmt.setInt(3, age);
			pstmt.setString(4, email);

			boolean res = pstmt.execute();
			// System.out.println("Result is -----" + res);
			System.out.println("Student added...............");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static Student[] getstudent() {
		Connection conn = null;
		int count = countTable();
		Student[] student = new Student[count];
		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn.prepareStatement("select * from student");

			ResultSet rs = pstmt.executeQuery();
			int i = 0;
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int age = rs.getInt("age");
				String email = rs.getString("email");
				student[i++] = new Student(id, name, age, email);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return student;
	}

	public static int countTable() {
		Connection conn = null;
		int i = 0;
		try {
			conn = ConnectionBridge.getconnection();
			PreparedStatement pstmt = conn.prepareStatement("select count(*) from student");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				int count = rs.getInt(1);
				i = count;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return i;

	}

	public static void updateStudent(Scanner scn) {
		System.out.println("Inside updation");
		System.out.println("Enter unique id");
		int id = scn.nextInt();
		System.out.println("Enter email");
		String email = scn.next();
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();

			PreparedStatement pstmt = conn.prepareStatement("update student set email = ? where id =?");
			pstmt.setString(1, email);
			pstmt.setInt(2, id);

			int executeUpdate = pstmt.executeUpdate();
			if (executeUpdate == 1) {
				System.out.println("updated successfully");
			} else {
				System.out.println("updation failed");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteStudent(Scanner scn) {
		System.out.println("Inside deletion");
		System.out.println("Enter unique id");
		int id = scn.nextInt();
		Connection conn = null;
		try {
			conn = ConnectionBridge.getconnection();
			PreparedStatement pstmt = conn.prepareStatement("delete from users where id =?");
			pstmt.setInt(1, id);

			int executeUpdate = pstmt.executeUpdate();
			if (executeUpdate == 1) {
				System.out.println("Deletion successfully");
			} else {
				System.out.println("Deletion failed");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
